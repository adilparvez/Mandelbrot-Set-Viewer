Mandelbrot Set Viewer
=====================
---------------------

A Mandelbrot set viewer with smooth colouring. Use WASD keys to move around the C-plane. Left click to zoom in and right click to zoom out.

![screenshot](/mandelbrot_screenshot.png)

![](/mandelbrot.gif)

---------------------