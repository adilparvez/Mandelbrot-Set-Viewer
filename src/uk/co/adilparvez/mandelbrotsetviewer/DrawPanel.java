package uk.co.adilparvez.mandelbrotsetviewer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class DrawPanel extends JPanel implements KeyListener, MouseListener {
	
	private final int WIDTH = 800;
	private final int HEIGHT = 600;
	public int maxIterations = 100;
	
	private BufferedImage img = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	
	private double minRe = -2.2;
	private double maxRe = 0.8;
	private double minIm = -1.18;
	private double maxIm = minIm + (maxRe - minRe) * HEIGHT / WIDTH;
	private double reScroll;
	private double imScroll;
	private double zoomFactor = 2;
	
	DrawPanel() {
		super();
		this.setSize(WIDTH, HEIGHT);
		this.setVisible(true);
		this.addKeyListener(this);
		this.addMouseListener(this);
		this.setFocusable(true);
		this.requestFocus();
	}

	private Pair<Boolean, Double> isInSetIterationsPair(ComplexNumber C) {
		ComplexNumber Z = new ComplexNumber(0, 0);
		ComplexNumber nextZ;
		for (int n = 0; n < maxIterations; n++) {
			nextZ = Z.multiplyBy(Z).add(C);
			Z.setEqualTo(nextZ);
			if (nextZ.abs() > 2) {
				double nSmooth = n + 1 - Math.log(Math.log(nextZ.abs())) / Math.log(2);
				return new Pair<Boolean, Double>(false, nSmooth);
			}
		}
		return new Pair<Boolean, Double>(true, new Double(maxIterations));
	}
	
	private int calculateColor(double iterations) {
		double ratio = iterations / maxIterations;
		return Color.HSBtoRGB((float)(ratio / 3), 1.0f, 1.0f);
	}
	
	private ComplexNumber coordinatesToComplexNumber(int x, int y) {
		double real = minRe + x * (maxRe - minRe) / (WIDTH - 1);
		double imaginary = maxIm - y * (maxIm - minIm) / (HEIGHT - 1);
		return new ComplexNumber(real, imaginary);
	}
	
	private void generateMandelbrot() {
		for (int x = 0; x < WIDTH; x++) {
			for (int y = 0; y < HEIGHT; y++) {
				ComplexNumber C = coordinatesToComplexNumber(x, y);
				Pair<Boolean, Double> pair = isInSetIterationsPair(C);
				if (!pair.first()) {
					int color = calculateColor(pair.second());
					img.setRGB(x, y, color);
				} else {
					img.setRGB(x, y, 0);
				}
			}
		}
	}
	
	private void zoomIn(ComplexNumber w) {
		double reRange = maxRe - minRe;
		double imRange = maxIm - minIm;
		
		minRe = w.re - reRange / (2 * Math.sqrt(zoomFactor));
		maxRe = w.re + reRange / (2 * Math.sqrt(zoomFactor));
		minIm = w.im - imRange / (2 * Math.sqrt(zoomFactor));
		maxIm = w.im + imRange / (2 * Math.sqrt(zoomFactor));
	}
	
	private void zoomOut(ComplexNumber w) {
		double reRange = maxRe - minRe;
		double imRange = maxIm - minIm;
		
		minRe = w.re - reRange * Math.sqrt(zoomFactor) / 2;
		maxRe = w.re + reRange * Math.sqrt(zoomFactor) / 2;
		minIm = w.im - imRange * Math.sqrt(zoomFactor) / 2;
		maxIm = w.im + imRange * Math.sqrt(zoomFactor) / 2;
	}
	
	public void increaseMaxIterations() {
		maxIterations += 100;
	}
	
	public void decreaseMaxIterations() {
		if (maxIterations > 100) {
			maxIterations -= 100;
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		generateMandelbrot();
		g.drawImage(img, 0, 0, null);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		reScroll = (maxRe - minRe) * 3 / 24;
		imScroll = (maxIm - minIm) * 3 / 32;
		if (e.getKeyCode() == KeyEvent.VK_W) {
			minIm += imScroll;
			maxIm += imScroll;
		}
		if (e.getKeyCode() == KeyEvent.VK_A) {
			minRe -= reScroll;
			maxRe -= reScroll;
		}
		if (e.getKeyCode() == KeyEvent.VK_S) {
			minIm -= imScroll;
			maxIm -= imScroll;
		}
		if (e.getKeyCode() == KeyEvent.VK_D) {
			minRe += reScroll;
			maxRe += reScroll;
		}
		generateMandelbrot();
		repaint();
	}

	@Override
	public void keyReleased(KeyEvent e) {}

	@Override
	public void keyTyped(KeyEvent e) {}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			ComplexNumber numberUnderMousePos = coordinatesToComplexNumber(e.getX(), e.getY());
			zoomIn(numberUnderMousePos);
		}
		if (SwingUtilities.isRightMouseButton(e)) {
			ComplexNumber numberUnderMousePos = coordinatesToComplexNumber(e.getX(), e.getY());
			zoomOut(numberUnderMousePos);
		}
		generateMandelbrot();
		repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {}

}
