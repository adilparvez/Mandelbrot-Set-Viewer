package uk.co.adilparvez.mandelbrotsetviewer;

public class ComplexNumber {
	
	public double re;
	public double im;
	
	ComplexNumber(double re, double im) {
		this.re = re;
		this.im = im;
	}
	
	public double abs() {
		return Math.sqrt(re * re + im * im);
	}
	
	public double absSquared() {
		return re * re + im * im;
	}
	
	public ComplexNumber add(ComplexNumber w) {
		return new ComplexNumber(re + w.re, im + w.im);
	}
	
	public ComplexNumber subtract(ComplexNumber w) {
		return new ComplexNumber(re - w.re, im - w.im);
	}
	
	public ComplexNumber multiplyBy(ComplexNumber w) {
		return new ComplexNumber(re * w.re - im * w.im, re * w.im + im * w.re);
	}
	
	public ComplexNumber reciprocal() {
		double lengthSquared = this.absSquared();
		return new ComplexNumber(re / lengthSquared, -im / lengthSquared);
	}
	
	public ComplexNumber divideBy(ComplexNumber w) {
		return this.multiplyBy(w.reciprocal());
	}
	
	public void setEqualTo(ComplexNumber w) {
		re = w.re;
		im = w.im;
	}
	
	@Override
	public String toString() {
		if (im == 0) {
			return re + "";
		} else if (re == 0) {
			return im + "i";
		} else if (im < 0) {
			return re + "-" + (-im) + "i";
		} else {
			return re + "+" + im + "i";
		}
	}
	
}
