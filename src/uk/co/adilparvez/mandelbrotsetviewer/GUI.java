package uk.co.adilparvez.mandelbrotsetviewer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GUI extends JFrame {
	
	private final int WIDTH = 800;
	private final int HEIGHT = 650;
	private final String title = "Mandelbrot Set Viewer";
	
	public void go() {
		this.setTitle(title);
		this.setResizable(false);
		this.setSize(WIDTH, HEIGHT);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		
		JPanel content = new JPanel();
		content.setSize(WIDTH, HEIGHT);
		content.setLayout(null);
		
		//====================================================================
		
		final DrawPanel drawPanel = new DrawPanel();
		drawPanel.setLocation(0, 0);
		content.add(drawPanel);
		
		//====================================================================
		
		final JLabel maxIterationsLabel = new JLabel("Max iterations : " + drawPanel.maxIterations);
		maxIterationsLabel.setLocation(175, 600);
		maxIterationsLabel.setSize(150, 22);
		content.add(maxIterationsLabel);
		
		//====================================================================

		JButton decreaseMaxIterationsButton = new JButton("-");
		decreaseMaxIterationsButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				drawPanel.decreaseMaxIterations();
				maxIterationsLabel.setText("Max iterations : " + drawPanel.maxIterations);
			}
			
		});
		decreaseMaxIterationsButton.setLocation(325, 600);
		decreaseMaxIterationsButton.setSize(100, 22);
		content.add(decreaseMaxIterationsButton);
		
		//====================================================================
		
		JButton increaseMaxIterationsButton = new JButton("+");
		increaseMaxIterationsButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				drawPanel.increaseMaxIterations();
				maxIterationsLabel.setText("Max iterations : " + drawPanel.maxIterations);
			}
			
		});
		increaseMaxIterationsButton.setLocation(425, 600);
		increaseMaxIterationsButton.setSize(100, 22);
		content.add(increaseMaxIterationsButton);
		
		//====================================================================
		
		JButton refresh  = new JButton("Refresh");
		refresh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				drawPanel.repaint();
			}
			
		});
		refresh.setLocation(525, 600);
		refresh.setSize(100, 22);
		content.add(refresh);
		
		//====================================================================
		
		this.setLocationRelativeTo(null);
		this.getContentPane().add(content);
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		GUI gui = new GUI();
		gui.go();
	}

}
