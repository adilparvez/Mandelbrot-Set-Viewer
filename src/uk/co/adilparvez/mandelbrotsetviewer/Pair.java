package uk.co.adilparvez.mandelbrotsetviewer;

public class Pair<X, Y> {
	
	private X first;
	private Y second;
	
	Pair(X first, Y second) {
		this.first = first;
		this.second = second;
	}
	
	public X first() {
		return first;
	}
	
	public Y second() {
		return second;
	}

}
